from django.urls import re_path
from GetPolicies.views import getPoliciesApi

urlpatterns=[
    re_path(r'^Policies',getPoliciesApi),
    re_path(r'^Policies/%spolicyId=([0-9]+)$',getPoliciesApi),
    re_path(r'^Policies/%scustomerId=([0-9]+)$',getPoliciesApi)
]