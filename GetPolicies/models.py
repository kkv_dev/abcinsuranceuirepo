from django.db import models

# Create your models here.

class Policies(models.Model):
    POLICY_ID= models.IntegerField()
    DATE_OF_PURCHASE= models.DateField()
    PREMIUM= models.IntegerField()
    BODILY_INJURY_LIABILITY= models.IntegerField()
    PERSONAL_INJURY_LIABILITY= models.IntegerField()
    COLLISION= models.IntegerField()
    COMPREHENSIVE= models.IntegerField()
    PROPERTY_DAMAGE_LIABILITY= models.IntegerField()
    VEHICLE_SEGMENT = models.CharField(max_length=10)
    FUEL=models.CharField(max_length=10)
    CUSTOMER_ID = models.IntegerField()
    CUSTOMER_REGION = models.CharField(max_length=20)
    
