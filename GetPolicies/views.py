from email.policy import default
import imp
from GetPolicies import serializer
from GetPolicies.models import Policies
from GetPolicies.serializer import PolicySerializer
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse
from django.db import connection


# Create your views here.

@csrf_exempt
def getPoliciesApi(request, policyId=None, customerId=None):
    policyId = request.GET.get('policyId', default= None)
    customerId = request.GET.get('customerId', default= None)
    cursor = connection.cursor()
    if (policyId is None) and (customerId is None):
        list =[]
        cursor.execute('EXEC [dbo].[spGetPolicyDetails]')
        result_set = cursor.fetchall()
        for row in result_set:
            Policy_serializedResult = PolicySerializer({'POLICY_ID': row[0],'DATE_OF_PURCHASE': row[1], 'PREMIUM': row[2], 'BODILY_INJURY_LIABILITY': row[3], 'PERSONAL_INJURY_LIABILITY': row[4], 'COLLISION':row[5], 'COMPREHENSIVE':row[6], 'PROPERTY_DAMAGE_LIABILITY':row[7], 'VEHICLE_SEGMENT':row[8], 'FUEL':row[9], 'CUSTOMER_ID':row[10], 'CUSTOMER_REGION': row[11]})
            list.append(Policy_serializedResult.data)
        cursor.close()
        return JsonResponse(list, safe=False)
    if (policyId is not None) and (customerId is None):
        list =[]
        cursor.execute('EXEC [dbo].[spGetPolicyDetails] @policyId='+ str(policyId))
        result_set = cursor.fetchall()
        for row in result_set:
            Policy_serializedResult = PolicySerializer({'POLICY_ID': row[0],'DATE_OF_PURCHASE': row[1], 'PREMIUM': row[2], 'BODILY_INJURY_LIABILITY': row[3], 'PERSONAL_INJURY_LIABILITY': row[4], 'COLLISION':row[5], 'COMPREHENSIVE':row[6], 'PROPERTY_DAMAGE_LIABILITY':row[7], 'VEHICLE_SEGMENT':row[8], 'FUEL':row[9], 'CUSTOMER_ID':row[10], 'CUSTOMER_REGION': row[11]})
            list.append(Policy_serializedResult.data)
        cursor.close()
        return JsonResponse(list, safe=False)
    if (policyId is None) and (customerId is not None):
        list =[]
        cursor.execute('EXEC [dbo].[spGetPolicyDetails] @customerId='+ str(customerId))
        result_set = cursor.fetchall()
        for row in result_set:
            Policy_serializedResult = PolicySerializer({'POLICY_ID': row[0],'DATE_OF_PURCHASE': row[1], 'PREMIUM': row[2], 'BODILY_INJURY_LIABILITY': row[3], 'PERSONAL_INJURY_LIABILITY': row[4], 'COLLISION':row[5], 'COMPREHENSIVE':row[6], 'PROPERTY_DAMAGE_LIABILITY':row[7], 'VEHICLE_SEGMENT':row[8], 'FUEL':row[9], 'CUSTOMER_ID':row[10], 'CUSTOMER_REGION': row[11]})
            list.append(Policy_serializedResult.data)
        cursor.close()
        return JsonResponse(list, safe=False)
    
