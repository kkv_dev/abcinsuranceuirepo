from django.apps import AppConfig


class GetpoliciesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'GetPolicies'
