from dataclasses import fields
from rest_framework import serializers
from GetPolicies.models import Policies

class PolicySerializer(serializers.ModelSerializer):
    class Meta:
        model = Policies
        fields = '__all__'